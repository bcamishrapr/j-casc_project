#!/bin/bash
#Script For Constructing YAML File with Txt File
#TEST3=`cat my.txt|awk '{print $3}'|tail -n 1`
echo "Script Started"

FILE_NAME="users.txt"
BACKUP_FILE="users1.txt"

cat $FILE_NAME | while read line; 
do
#echo $line;
REALM=$(echo $line | awk '{print $1}')
PERMIT=$(echo $line | awk '{print $2}')
USER=$(echo $line | awk '{print $3}')
#echo $REALM

#Putting the data inside the YAML reading line-by-line from text
echo -e "\t - \"$REALM/$PERMIT:$USER\"" >> jenkins.yaml

done 

echo "------$(date +%F-%T)------" >> total_user_list.txt

#cp -rvf my.txt my-total-list.txt
cat users.txt >> total_user_list.txt

#mv $FILE_NAME $BACKUP_FILE && touch $FILE_NAME
echo "Script End"
