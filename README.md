# J-CASC_project

Demo for Jenkins Configuration as a code

## Requirements
- Configuration as Code Jenkins Plugin
- For Role Based Strategy (curl cmd not working with user&pass)
- Resolution: - Install below plugin first in jenkins
1. Build Authorization TOken Root Plugin --> This is used to run the job without user&pass, it uses job token only.
2. Groovy --> This is used to  direct execute groovy code

- Then We need to define one job is jenkins which update the user and roles file in jenkins and then run next job(groovy_code) to reload  the JCASC config
```
     import io.jenkins.plugins.casc.ConfigurationAsCode;
try{
println "Reloading Configuration-As-A-Code";
ConfigurationAsCode.get().configure();
println "Re-Loaded Successfully";
}catch(Exception e){
logger("something occurred "+e)
   return
}
```
---
[JCASC-REF (https://verifa.io/insights/getting-started-with-jenkins-config-as-code/)
